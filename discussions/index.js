// Node.js Introduction

// Use the "require" directive load Node.js modules
// The "http module" lets Node.js transfer data using the HTTP
// HTTP os a protocol that allows the fetching of resources such as HTML docs.
// The message sent by the "client", usually a Web browser called "request"
// The message sent by the "server" as an answer are called "responses"
let http = require("http");

//  Using this module's createServer() method, we can create an HTTP server that listen to request on a specified port and gives responses back to client.

// A port is a virtual point where network connection starts and end. Each port is associated with a specific process or server

/*

http.createServer(function (request, response) {

	// Use the writeHead() method to:
	//  Set a status code for the response - a 200 means OK
	// Set the content-type of the 

	response.writeHead(200, {'Content-Type' : 'text/plain'});

	response.end("Welcome to my Life - Paparoach ft Snoop Dog");

}).listen(4000)

// when serve is running, console will print the message:
console.log('Server is running at localhost:4000');

*/

// With conditionals

const server = http.createServer((request, response) => {

	// Accessing the "greeting" route returns message of "Welcome to BPI"

	// "request" is object that is sent via the client(browser)

	// the "url" property refers to the url or link in the browser.

	if(request.url == '/greeting'){
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("Welcome to BPI");
	}else if(request.url == '/Cus') {

		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("Welcome to Customer Service of BPI");
	}else{
		response.writeHead(404, {'Content-Type' : 'text/plain'});
		response.end("Page is not available")
	}
}).listen(4000);

console.log('Server running at localhost:4000');